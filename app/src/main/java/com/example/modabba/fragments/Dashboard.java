package com.example.modabba.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.modabba.AddressActivity;
import com.example.modabba.MainActivity;
import com.example.modabba.R;
import com.example.modabba.ViewPagerAdapter;
import com.google.android.material.chip.Chip;
import com.google.android.material.tabs.TabLayout;
import com.huxq17.swipecardsview.SwipeCardsView;
import com.kofigyan.stateprogressbar.StateProgressBar;

public class Dashboard extends Fragment {
    String[] descriptionData = {"Preparing", "On Way", "Delivered"};
    private SwipeCardsView swipeCardsView;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);
        StateProgressBar stateProgressBar = v.findViewById(R.id.progress_bar);
        stateProgressBar.setStateDescriptionData(descriptionData);
        return v;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText("Veg"));
        tabLayout.addTab(tabLayout.newTab().setText("Non Veg"));
        tabLayout.setupWithViewPager(viewPager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(),2);
        viewPager.setAdapter(viewPagerAdapter);

    }
}
